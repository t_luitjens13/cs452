#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include "lex.h" 
#include <string.h>

//#define DBUG

char** getaline();
int andersand(char **args);
int orsand(char **args);

/*
* Handle exit signals from child processes
*/
void sig_handler(int signal) {
	int status;
	
	int result = wait(&status);
	printf("Wait returned %d\n", result);
}
/*
* The main shell function
*/ 
main() {
	int i, result, block, output, input;
	char** args; 
	char *output_filename;
	char *input_filename;
	
	
	// Set up the signal handler
	sigset(SIGCHLD, sig_handler);
	
	sigignore(SIGCHLD);	
	sigignore(SIGTTIN);
	sigignore(SIGTTOU);

	// Loop forever
	while(1) {
		
		// Print out the prompt and get the input
		printf("->");
		args = getaline();
		
		// No input, continue
		if(args[0] == NULL)
			continue;
		
		// Check for internal shell commands, such as exit
		if(internalCommand(args))
			continue;
		
		// Check for an ampersand
		block = (ampersand(args) == 0);
		
		// Check for redirected input
		input = redirect_input(args, &input_filename);
		
		switch(input) {
		case -1:
			printf("Syntax error!\n");
			continue;
			break;
		case 0:
			break;
		case 1:
			printf("Redirecting input from: %s\n", input_filename);
			break;
		}
		
		// Check for redirected output
		output = redirect_output(args, &output_filename);
		
		switch(output) {
		case -1:
			printf("Syntax error!\n");
			continue;
			break;
		case 0:
			break;
		case 1:
			printf("Redirecting output to: %s\n", output_filename);
			break;
		}
		
		//check for special cases
		if(andersand(args) || orsand(args) || semiCheck(args)){
			doSpecialCommand(args, block, input, input_filename, output, output_filename);
		}
		else if(pipesExist(args)){
			doPipe(args, block, input, input_filename, output, output_filename);
			#ifdef DBUG
			printf("After the fact\n");
			#endif
		}
		else{
			doCommand(args, block, input, input_filename, output, output_filename);
		}
	}
}

/*
* Check for ampersand as the last argument
*/
int ampersand(char **args) {
  int i;

  for(i = 1; args[i] != NULL; i++) ;

  if(args[i-1][0] == '&') {
    free(args[i-1]);
    args[i-1] = NULL;
    return 1;
  } 
  
  return 0;
}

/* 
* Check for internal commands
* Returns true if there is more to do, false otherwise 
*/
int internalCommand(char **args) {
	if(strcmp(args[0], "exit") == 0) {
		exit(0);
	}
	if(strcmp(args[0], "e") == 0) {
		exit(0);
	}
	
	return 0;
}

/*
* Checks to see if there is 
*
*/
int andersand(char **args){
	if(!ampersand(args)){
		int i = 0;
		for(i=0; args[i]!=NULL; i++){
			if(strcmp(args[i], "&") == 0 && strcmp(args[i+1], "&") == 0){
				return 1;
			}
		}
	}
	return 0;
}


int orsand(char **args){
	int i = 0;
	for(i=0; args[i]!=NULL; i++){
		if(strcmp(args[i], "|") == 0 && strcmp(args[i+1], "|") == 0){
			return 1;
		}
	}
	return 0;
}

int semiCheck(char **args){
	int i = 0;
	for(i=0; args[i]!=NULL; i++){
		if(strcmp(args[i], ";") == 0){
			return 1;
		}
	}
	return 0;
}

int pipesExist(char **args){
	int i = 0;
	for(i=0; args[i]!=NULL; i++){
		if(strcmp(args[i], "|") == 0){
			return 1;
		}
	}
	return 0;
}
int doSpecialCommand(char **args, int block, int input, char *input_filename, int output, char *output_filename){
	int i = 0;
	int j;
	int result = 0;
	while(args[i] != NULL  && !result){
		result = 0;
		
		char** buffer = malloc(sizeof(args));
		j = 0;
	
		for(i; args[i]!=NULL && (strcmp(args[i],"&") != 0 && strcmp(args[i],"|") != 0 && strcmp(args[i],";") != 0); i++, j++){
				buffer[j] = args[i];
		}
		#ifdef DBUG
		printf("--Buffer at 0 : %s\n", buffer[0]);
		#endif
		result = doCommand(buffer, block, input, input_filename, output, output_filename);
		#ifdef DBUG
		printf("%d\n",result);
		#endif
		
		if(args[i]!=NULL && strcmp(args[i],"&") == 0){ //And - Stop at first False thing
			if(WIFSIGNALED(result)==0){//fails
				#ifdef dbug
				printf("WIFEXTED result: %d\n",WIFSIGNALED(result));
				#endif
				result = 1;
			}else{
				result = 0;
			}
		}
		else if(args[i]!=NULL && strcmp(args[i],"|") == 0){ //Or - Stop at first True thing
			if(!result){//passes
				result = 1;
			}else{
				result = 0;
			}
		}
		else if(args[i]!=NULL && strcmp(args[i],";") == 0){ //Separate commands
			i--;
			result = 0;//continue
		}
		
		
		i+=2;
		free(buffer);
	}
}

/* 
* Do the command
*/
int doCommand(char **args, int block, int input, char *input_filename, int output, char *output_filename) {

		int result = 0;
		pid_t child_id;
		int status;
		struct sigaction action;
		action.sa_handler = sig_handler;
		// Fork the child process
		child_id = fork();
		
		// Check for errors in fork()   
		switch(child_id) {
		case EAGAIN:
			perror("Error EAGAIN: ");
			return;
		case ENOMEM:
			perror("Error ENOMEM: ");
			return;
		}
		
		int pipeCount = 0;
		int i;
		for(i = 0; args[i] != NULL; i++){
			#ifdef DBUG
			printf("( %s)\n",args[i]);
			#endif
			if(strcmp(args[i], "|")==0){
				//check for concurrent pipes here
				pipeCount++;
			}
		}
		#ifdef DBUG
		printf("~~0~~\n");
		#endif
		
		//check to see if we're in the child process
		if(child_id == 0) { 
			if(!block){
			setpgid(child_id, child_id);
			}
			
			action.sa_handler=SIG_DFL;
			sigaction(SIGINT, &action, NULL);
			sigaction(SIGCHLD, &action, NULL);
			sigaction(SIGQUIT, &action, NULL);
			sigaction(SIGCONT, &action, NULL);
			sigaction(SIGSTOP, &action, NULL);
			sigaction(SIGTTIN, &action, NULL);
			sigaction(SIGTTOU, &action, NULL);
			// Set up redirection in the child process
			if(input)
				freopen(input_filename, "r", stdin);     
			if(output==1)
				freopen(output_filename, "w+", stdout);
			if(output == 2){
				freopen(output_filename, "a+",stdout);
			}
			
			// Execute the command
			execvp(args[0], args);
			exit(-1); //gets to here if execvp fails due to unknown commands
			
		}else if(!block){
			#ifdef DBUG
			printf("In the parent and should be backgrounding\n");
			#endif
			setpgid(child_id,0);
			tcsetpgrp(STDIN_FILENO, getpgrp());
		}
		
		// Wait for the child process to complete, if necessary
		if(block) {
			#ifdef DBUG
			printf("Blocked\n");
			printf("Waiting for child, pid = %d\n", child_id);
			#endif
			waitpid(child_id, &status, 0);
		}

		return status;
	}            
	
	/*
	* Check for input redirection
	*/
	int redirect_input(char **args, char **input_filename) {
		int i;
		int j;
		
		for(i = 0; args[i] != NULL; i++) {
			
			// Look for the <
			if(args[i][0] == '<') {
				free(args[i]);
				
				// Read the filename
				if(args[i+1] != NULL) {
					*input_filename = args[i+1];
				} else {
					return -1;
				}
				
				// Adjust the rest of the arguments in the array
				for(j = i; args[j-1] != NULL; j++) {
					args[j] = args[j+2];
				}
				
				return 1;
			}
		}
		
		return 0;
	}
	
	/*
	* Check for output redirection
	*/
	int redirect_output(char **args, char **output_filename) {
		int i;
		int j;
		
		for(i = 0; args[i] != NULL; i++) {
			
			// Look for the >
			if(args[i][0] == '>') {
				free(args[i]);
				
				// Get the filename 
				if(args[i+1] != NULL) {
					
					//append
					if(args[i+1][0]=='>' && args[i+2]!=NULL){
						*output_filename = args[i+2];
						for(j = i; args[j-1] != NULL; j++) {
							args[j] = args[j+3];
						}
						return 2;
					}
					
					*output_filename = args[i+1];
					
				} else {
					return -1;
				}
				
				// Adjust the rest of the arguments in the array
				for(j = i; args[j-1] != NULL; j++) {
					args[j] = args[j+2];
				}
				
				return 1;
			}
		}
		
		return 0;                     
	}
	
	int doPipe(char **args, int block, int input, char *input_filename, int output, char *output_filename){
		int i=0;
		int j, status;
	
		while(args[i+1] != NULL){ //delimit by pipes for each command
			j=0;
			char** buffer = malloc(sizeof(args));
			for(i; args[i]!=NULL && strcmp(args[i],"|") != 0; i++, j++){
				buffer[j] = args[i];
			}
			int pipes[2];
			pipe(pipes);
			int child_id = fork();
			if(child_id==0){
				dup2(pipes[1],1);
				doCommand(buffer, block, input, input_filename,output, output_filename);
				
				exit(-1);
			}
			
			dup2(pipes[0],0);
			close(pipes[1]);
		
			i++;
			//free(buffer);
			int y;
			for(y=0; args[y]!=NULL;y++){
				buffer[y] = NULL;
			}
		}
		char** buffer = malloc(sizeof(args));
		
		j=0;
		for(i; args[i]!=NULL && strcmp(args[i],"|") != 0; i++, j++){
			buffer[j] = args[i];
		}
		doCommand(buffer, block, input, input_filename,output, output_filename); //need to copy to buffer one more time here
		
		#ifdef DBUG
		printf("After the final doCommand\n");
		#endif

		return 0;
	}
		