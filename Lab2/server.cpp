/*
	change ints to uint32
	send as a list
	fork print statements
	generate initial list smaller
	send in chunks?
*/
using namespace std;

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <vector>
#include <stdint.h>

typedef uint32_t uintt;

vector<uintt> makeList(uintt lim);
vector<uintt> removePrimeMultiples(uintt x, vector<uintt> list);
int main(){
	vector<uintt> primeList;
	uintt limit = 10000;
	
	uintt portNum = 9633;
	uintt sockfd = socket(AF_INET, SOCK_STREAM, 0); //domain, type, protocol
	uintt receive[1000];
	uintt toSend[1000];
	if(sockfd <0){
		perror("SOCKET CREATION FAILED");
	}
	
	struct sockaddr_in serv_addr, cli_addr;
	bzero((char*)&serv_addr, sizeof(serv_addr)); //clears out the struct
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(portNum);
	
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
		perror("ERROR ON BINDING");
	}
	
	listen(sockfd, 5);
	socklen_t clilen = sizeof(cli_addr);
	uintt newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr, &clilen);
	
	if(newsockfd<0){
		perror("ERROR ON ACCEPT");
	}
	else{
		cout <<"Client found.\nConnection Succesful\n";
	//cout<<"Server got connection from "<< inet_ntoa(cli_addr.sin_addr) << " on port: " << ntohs(cli_addr.sin_port) << "\n";
	}
	// send(sockfd, whatToSend, sizeof(whatSending), 0);
	//read(newsockfd, receive, sizeof(recieve)-1);
	//uintt status=send(sockfd, static_cast<void*>(fullList.data()), sizeof(fullList), 0);
	vector<uintt> fullList = makeList(limit);
	uintt tempSize = /*htonl*/(limit -1);
	send(newsockfd, &tempSize, sizeof(limit), 0);
	for(uintt i : fullList){
		uintt toSend = /*htonl*/(i);
		uintt status=send(newsockfd, &toSend ,sizeof(fullList[i]), 0);
		if(status<0)
			perror("ERROR ON SENDING ARRAY");
	}
	
	uintt listSize=-1;
	do{ 
	
		
		vector<uintt> inList;
		vector<uintt> sendBack;
		//cout << "Waiting to read \n";
		uintt status =read(newsockfd,&listSize, sizeof(listSize)); //get list size
		if(status < 0){
			perror("Some shit broke");
		}
		//cout << "List Size: " << /*ntohl*/(listSize) << "\n";
		if(status<0){
				perror("ERROR ON RECIEVE");
			}
		//cout << "List Size: " << /*ntohl*/(listSize) << "\n";
		
		for(uintt i=0;i</*ntohl*/(listSize);i++){ //recieve the list
			uintt temp = -1;
			uintt status = read(newsockfd,&temp, sizeof(temp)); 
			if(status<0){
				perror("ERROR ON RECIEVE");
			}
			else{
				inList.push_back(/*ntohl*/(temp));
			}
		}
		if(inList.size()>5)
			printf("Recvd: %d , %d, %d ... %d, %d, %d\n", inList[0], inList[1], inList[2], inList[inList.size()-3],inList[inList.size()-2],inList[inList.size()-1]);
		else if(inList.size() > 0){
			printf("Recvd: ");
			for(int x : inList){
				printf("%d ", x);
			}
			printf("\n");
		}
		//cout << "HELLO FROM THE OTHER SIDE\n";
		//remove prime
		
		primeList.push_back(inList[0]);
		sendBack = removePrimeMultiples(inList[0], inList); 
		cout << "Prime: " << inList[0] << "\n";
		//cout << "HELLO FROM THIS SIDE\n";
		uintt size = /*htonl*/(sendBack.size());//send the size of the list
		send(newsockfd, &size, sizeof(size), 0);
		if(sendBack.size()>5)
			printf("Sent: %d , %d, %d ... %d, %d, %d\n\n", sendBack[0], sendBack[1], sendBack[2], sendBack[sendBack.size()-3],sendBack[sendBack.size()-2],sendBack[sendBack.size()-1]);
		else if(sendBack.size() > 0){
			printf("Sent: ");
			for(int x : sendBack){
				printf("%d ", x);
			}
			printf("\n\n");
		}
		for(uintt x : sendBack){
			
			uintt sending = /*htonl*/(x);
			send(newsockfd, &sending ,sizeof(x), 0);
		}
		
		
		if(sendBack.size() < 2)
			listSize = 0;
		//clean lists
		sendBack.clear();
		inList.clear();
		//cout << "Read complete\n";
	}while(/*ntohl*/(listSize));	
	
	//recieve the length of client's prime list
	uintt numClientPrimes;
	read(newsockfd,&numClientPrimes, sizeof(numClientPrimes));
	
	//recieve the list
	vector<uintt> clientList;
	for(uintt i=0;i</*ntohl*/(numClientPrimes);i++){
		uintt temp;
		read(newsockfd,&temp, sizeof(temp));
		clientList.push_back(/*ntohl*/(temp));
	}
	
	for(uintt i=0;i< /*ntohl*/(numClientPrimes); i++){
		cout << /*ntohl*/(clientList[i]) << ",";
		if(primeList[i] != NULL)
			cout << primeList[i] << ",";
	}
	
	cout << "\n";
	close(newsockfd);
	close(sockfd);
	return 0;
}

vector<uintt> makeList(uintt lim){
	vector<uintt> primes;
	for(uintt i=2; i<= lim;i++){
		primes.push_back(i);
	}
	return primes;
}

vector<uintt> removePrimeMultiples(uintt prime, vector<uintt> list){
	vector<uintt> revisedList;
	for(uintt x : list){
		if(x % prime !=0)
			revisedList.push_back(x);
	}
	return revisedList;
}
