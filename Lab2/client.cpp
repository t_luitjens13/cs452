using namespace std;

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <vector>
#include <stdint.h>

typedef uint32_t uintt;
vector<uintt> removePrimeMultiples(uintt prime, vector<uintt> list){
	vector<uintt> revisedList;
	for(uintt x : list){
		if(x % prime !=0)
			revisedList.push_back(x);
	}
	return revisedList;
}
int main(int argc, char* argv[]){
	
	vector<uintt> primeList;
	
	if(argc < 2){
		perror("NOT ENOUGH ARGUMENTS YOU FOOL");
		exit(1);
	}
	
	uintt portNum = 9633;
	uintt sockfd = socket(AF_INET, SOCK_STREAM, 0); //domain, type, protocol
	struct hostent* server;
	struct sockaddr_in serv_addr;
	if(sockfd <0){
		perror("SOCKET CREATION FAILED");
	}
	
	server = gethostbyname(argv[1]);
	if (server == NULL) {
        	perror("ERROR: NO SUCH HOST. GIVING UP...");
        	exit(0);
    	}
	
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_port = htons(portNum);
	
	if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
		perror("ERROR CONNECTING");
	}
	
	
	
	uintt listSize;
	do{ 
		vector<uintt> inList;
		vector<uintt> sendBack;
		//cout << "Waiting to read \n";
		read(sockfd,&listSize, sizeof(listSize)); //get list size
		//cout << "List Size: " << listSize << "\n";
		for(uintt i=0;i</*ntohl*/(listSize);i++){ //recieve the list
			uintt temp = -1;
			uintt status = read(sockfd,&temp, sizeof(temp)); //first time is going to be the list size
			if(status<0){
				perror("ERROR ON RECIEVE");
			}
			else{
				//cout << "Read: " << /*ntohl*/(temp) << "\n";
				inList.push_back(/*ntohl*/(temp));
			}
		}
		if(inList.size()>5)
			printf("Recvd: %d , %d, %d ... %d, %d, %d\n", inList[0], inList[1], inList[2], inList[inList.size()-3],inList[inList.size()-2],inList[inList.size()-1]);
		else if(inList.size() > 0){
			printf("Recvd: ");
			for(int x : inList){
				printf("%d ", x);
			}
			printf("\n");
		}
		//remove prime
		primeList.push_back(/*ntohl*/(inList[0]));
		sendBack = removePrimeMultiples(inList[0], inList); 
		cout << "Prime: " << inList[0] << "\n";
		uintt size = /*htonl*/(sendBack.size());//send the size of the list
		//cout << "Size: " << /*ntohl*/(size) << "\n";
		send(sockfd, &size, sizeof(size), 0);
		if(sendBack.size()>5)
			printf("Sent: %d , %d, %d ... %d, %d, %d\n\n", sendBack[0], sendBack[1], sendBack[2], sendBack[sendBack.size()-3],sendBack[sendBack.size()-2],sendBack[sendBack.size()-1]);
		else if(sendBack.size() > 0){
			printf("Sent: ");
			for(int x : sendBack){
				printf("%d ", x);
			}
			printf("\n\n");
		}
		for(uintt x : sendBack){
			uintt sending = /*htonl*/(x);
			send(sockfd, &sending ,sizeof(x), 0);
		}
		
	
		if(sendBack.size() < 2)
			listSize = 0;
		//clean lists
		sendBack.clear();
		inList.clear();
		//cout << "Read complete\n";
		
	}while(/*ntohl*/(listSize));
	
	//send back size of primeList
	uintt numPrimes = /*htonl*/(primeList.size());
	send(sockfd, &numPrimes ,sizeof(numPrimes), 0);
	
	//send back all of the primes
	for(uintt x : primeList){
			uintt sending = /*htonl*/(x);
			send(sockfd, &sending ,sizeof(x), 0);
		}
		
	close(sockfd);
	return 0;
}
