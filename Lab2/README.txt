TO RUN: make clean
	make
	./server
	./client thing1.cs.uwec.edu     (assuming server is being run on thing1, change as needed)

Results are found within the serverRun and clientRun txt files for a list of 10,000 numbers
Timing for 1,000,000 is found in TimeResult1M.png
Memory leak testing for 100,000 numbers is found in ValgrindResult100k.png


