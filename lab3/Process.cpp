#include "Process.h"
using namespace std;
//starting how many cycles have been run thru and waiting

Process::Process(int npid, int nburst, int narrival, int npriority, int ndeadline, int nio){
	pid = npid;
	burst = nburst;
	arrival = narrival;
	priority = npriority;
	deadline = ndeadline;
	io = nio;
	waitTime = 0;
	activeTime = 0;
	status = "Not completed";
	age = 0;
	nextProcess = NULL;
}
int Process::getPid(){
	return pid;
}
int Process::getArrival(){
	return arrival;
}
int Process::getWaitTime(){
	return waitTime;
}
void Process::updateWaitTime(int timing){
	waitTime = timing;
}
void Process::updateWaitTime(){
	waitTime ++;
}

int Process::getActiveTime(){
	return activeTime;
}
	
void Process::updateActiveTime(int timing){
	activeTime += timing;
}
void Process::updateActiveTime(){
	activeTime ++;
}

int Process::getPriority(){
	return priority;
}

void Process::setPriority(int pri){
	priority = pri;
}
int Process::getDeadline(){
	return deadline;
}

int Process::getIO(){
	return io;
}

void Process::setIO(int newIo){
	io = newIo;
}

int Process::getBurst(){
	return burst;
}

string Process::getStatus(){
	return status;
}

void Process::setStatus(string newStatus){
	status = newStatus;
}
void Process::setStartTime(int tick){
	starting = tick;
}
int Process::getStartTime(){
	return starting;
}
void Process::next(Process* process){
	nextProcess = process;
}

void Process::updateAge(){
	age++;
}

void Process::updateAge(int aging){
	age += aging;
}

void Process::resetAge(){
	age = 0;
}
int Process::getAge(){
	return age;
}
bool Process::canFinish(int cycle){
	return deadline - cycle - burst + activeTime >= 0;
}
Process* Process::next(){
	return nextProcess;
}
bool Process::done(){
	return burst <= activeTime;
}
void Process::toString(){
	cout<<"ID: "<<pid;
	cout<<", Arrival: " << arrival;
	cout<<", Burst: " << burst;
	cout<<", Wait time: " << waitTime;
	cout<< ", Active time: "<< activeTime;
	cout<<", Priority: " << priority;
	cout<<", Deadline: " << deadline;
	cout<<", Status: " << status << "\n\n";
}

