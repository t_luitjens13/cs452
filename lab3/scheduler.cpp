#include "Process.h"
#define printProcesses
bool hasStuff(vector<queue<Process*>> levels){
	bool stuff = false;
	for(int i=0; i<levels.size() && stuff==false;i++){
		if( !levels.at(i).empty()){
			stuff = true;
		}
	}
	return stuff;	
}

vector<Process*> parseFile(string fileName){
	vector<Process*> processes;
	fstream dataFile;
	dataFile.open(fileName);
	int a, pid, burst, arrival, priority, deadline, io;
	int count = 0;
	while (dataFile >> a){
		
		if (count % 6 == 0)
			pid = a;
		else if (count % 6 == 1)
			burst = a;
		else if (count % 6 == 2)
			arrival = a;
		else if (count % 6 == 3)
			priority = a;
		else if (count % 6 == 4)
			deadline = a;
		else{
			io = a;
			if(arrival >= 0 && burst >= 0 && priority >=0 && deadline >=0 && io >= 0 && pid >= 0) //throws out any invalid processes
			processes.push_back(new Process(pid, burst, arrival, priority, deadline, io));
		}
		count++;
    }
	dataFile.close();
	return processes;
}

bool howToSort(Process* x, Process* y){
	if(x->getArrival() != y->getArrival())
		return x->getArrival() < y->getArrival();
	else if(x->getPriority() != y->getPriority())
		return x->getPriority() < y->getPriority();
	else
		return x->getPid() < y->getPid();
}
struct priorityCompare{
	bool operator()(Process* x, Process* y) const{
		return x->getIO() > y->getIO();
	}
};

struct deadLineCompare{
	bool operator()(Process* x, Process* y)const{
		return x->getDeadline() > y->getDeadline();
	}
};
vector<Process*> sortList(vector<Process*> processes){
	sort(processes.begin(), processes.end(), howToSort);
}

int changePriority(int Pbase, int Pjump){
	//0-49 and 50-99
	if(Pbase < 50){//0-49
		if(Pbase + Pjump > 49){
			return 49;
		}else if (Pbase + Pjump < 0){
			return 0;
		}
	}else{//50-99 (well 50+)
		if(Pbase + Pjump > 99){
			return 99;
		}else if (Pbase + Pjump < 50){
			return 50;
		}	
	}
	return Pbase + Pjump;
}

void rts(vector<Process*> processesIn){
	//TODO PQ sorted by deadline
	//TODO soft rts
	priority_queue<Process*, vector<Process*>, deadLineCompare> ready;
	queue<Process*> waiting;
	queue<Process*> completed;
	queue<Process*> deferred;
	int cycle = 0;
	cout << "Would you like a soft or hard environment? --> ";
	string environment;
	cin >> environment;
	
	//go through and delete any where the deadline is before the arrival
	for(int i=0; i<processesIn.size(); i++){
		if(processesIn.at(i)->getArrival() < processesIn.at(i)->getDeadline()) //check to make sure itll be completed
			waiting.push(processesIn.at(i));
	}

	
	while(!ready.empty() || !waiting.empty()){
		//add in new arrivals, go element by element since they are sorted by arrival
			while(waiting.front()!=NULL && waiting.front()->getArrival() <= cycle){
				if(waiting.front()->getDeadline() - waiting.front()->getBurst() >=cycle)
					ready.push(waiting.front());
				else{
					waiting.front()->setStatus("Deferred...");
					waiting.front()->updateWaitTime(cycle - waiting.front()->getArrival() - waiting.front()->getActiveTime()+1);
					deferred.push(waiting.front());
					if(environment.compare("hard") == 0){
						cout << "Aborted due to not meeting deadlines." << endl;
						exit(0);
					}
				}
				waiting.pop();
			}
			
			while(!ready.empty() && !ready.top()->canFinish(cycle)){
					ready.top()->setStatus("Deferred...");
					ready.top()->updateWaitTime(cycle - ready.top()->getArrival() - ready.top()->getActiveTime() +1);
					deferred.push(ready.top());
					if(environment.compare("hard") == 0){
						cout << "Aborted due to not meeting deadlines." << endl;
						exit(0);
					}
					ready.pop();
				}
			if(!ready.empty()){
				
				ready.top()->updateActiveTime();
				//Check if process is completed
				if(ready.top()->done()){
					ready.top()->setStatus("Completed!");
					ready.top()->updateWaitTime(cycle - ready.top()->getArrival() - ready.top()->getActiveTime()+1);
					completed.push(ready.top());
					ready.pop();
				}
			}
			cycle++;
			
			
	}
	//print out all of the processes with updated times
	double wt = 0;
	double tt = 0;
	int np = 0;
	int nc = 0;
	int nd = 0;
	while(!completed.empty()){
		np++;
		nc++;
		wt += completed.front()->getWaitTime();
		tt = tt + completed.front()->getWaitTime() + completed.front()->getActiveTime();
		#ifdef printProcesses
			completed.front()->toString();
		#endif
		completed.pop();
	}
	while(!deferred.empty()){
		np++;
		nd++;
		wt+=deferred.front()->getWaitTime();
		tt = tt + deferred.front()->getWaitTime() + completed.front()->getActiveTime();
		#ifdef printProcesses
			deferred.front()->toString();
		#endif
		deferred.pop();
		
	}
	wt = wt/np;
	tt = tt/np;
	cout << "Average Wait Time: " << wt << endl;
	cout << "Average Turnaround Time: " << tt << endl;
	cout << "Total Number of Processes Scheduled: " << np << endl;
	cout << "Processes completed: " << nc << endl;
	cout << "Processes deffered: " << nd << endl;
}

void mfqs(vector<Process*> processes){ 

	int numProcesses = 0;
	int quantum = 2;
	int ageInterval;
	queue<Process*> waiting;
	queue<Process*> completed;
	vector<queue<Process*>> levels;
	int cycle = 0;
	int numQueues = 0;
	cout << "How many queues would you like the scheduler to utilize (2 to 5) --> "; 
	cin >> numQueues;
	cout << "What would you like the quantum to be? --> ";
	cin >> quantum;
	cout << "What should the aging interval be? --> ";
	cin >> ageInterval;
	for(int i=0; i<numQueues-1; i++){ 
		levels.push_back(queue<Process*>());
	}
	for(Process* p : processes){
		waiting.push(p);
		numProcesses++;
	}
	
	Process* fcfsHead = new Process(-1,-1,-1,-1,-1,-1); //set to dummy process
	Process* fcfsTail = fcfsHead;
	
	while(!waiting.empty() || hasStuff(levels) || fcfsHead->next() != NULL){ 
		//check for new arrivals
		if(waiting.front() != NULL && waiting.front()->getArrival() <=cycle){
			waiting.front()->updateWaitTime(cycle - waiting.front()->getArrival());
			levels.at(0).push(waiting.front());
			waiting.pop();
		}
		
		bool didSomething = false;
		int lev;
		int tq = 1;
		for(int i = 0; i< levels.size();i++){
			if(!didSomething && !levels.at(i).empty()){
				//do the rr with (i+1)*TQ
				tq = quantum;
				for(int i=1;i<levels.size();i++){ //quantum * 2^i
					tq *=2;
				}
				if(tq + levels.at(i).front()->getActiveTime() >= levels.at(i).front()->getBurst()){ //means the process is done
					tq = levels.at(i).front()->getBurst() - levels.at(i).front()->getActiveTime();
				}
					
				
				levels.at(i).front()->updateActiveTime(tq);
				didSomething = true;
				lev = i;
			
			}
		}
		if(!didSomething){
			//FCFS
			if(fcfsHead->next() != NULL){
				//process goes which is burst - activeTime
				int tq = fcfsHead->next()->getBurst() - fcfsHead->next()->getActiveTime();
				
				fcfsHead->next()->updateActiveTime(tq);
				fcfsHead->next()->setStatus("Completed!");
				fcfsHead->next()->updateWaitTime(cycle + tq - fcfsHead->next()->getArrival() - fcfsHead->next()->getActiveTime());
				//fcfsHead->next()->toString();
				
				completed.push(fcfsHead->next());
				numProcesses --;
				
				fcfsHead->next(fcfsHead->next()->next());
			}
		}
		
		//age
		Process* temp = fcfsHead;
		while(temp != NULL && temp->next() != NULL){ //TODO up
			temp->next()->updateAge(tq);
			if(temp->next()->getAge() >= ageInterval){
				temp->next()->resetAge();
				levels.at(levels.size()-1).push(temp->next());
				
				temp->next(temp->next()->next());
			}
			else
				temp = temp->next();
			
		}
		fcfsTail = temp; //keep the tail at the last one
		
		//if not done, demote, if demoted to fcfs, make sure head not null and tail
		if(didSomething){
			//check if process is done
			if(levels.at(lev).front()->getActiveTime() >= levels.at(lev).front()->getBurst()){
				levels.at(lev).front()->setStatus("Completed!");
				levels.at(lev).front()->updateWaitTime(cycle - levels.at(lev).front()->getArrival() - levels.at(lev).front()->getActiveTime() +1);
				//levels.at(lev).front()->toString();
				completed.push(levels.at(lev).front());
				numProcesses --;
				levels.at(lev).pop();
			}
			//demote
			else{ 
				if(lev == levels.size() -1){
					if(fcfsHead->next() == NULL){
						fcfsHead->next(levels.at(lev).front());
						fcfsTail = fcfsHead->next();
					}
					else{
						fcfsTail->next(levels.at(lev).front());
						fcfsTail = fcfsTail->next();
					}
				}
				else{
					levels.at(lev+1).push(levels.at(lev).front());
				}

				levels.at(lev).pop();
			}
		}
		//when a process is finished, set wait time to cycle - activeTime - arrival
		
		
		cycle+=tq;
		//do clock ticks, must be a better way than iterating thru each of the queues? 
		//do queue changes
		//do arrivals, then promotions, then demotions
	}
	double wt = 0;
	double tt = 0;
	int np = 0;
	while(!completed.empty()){
		np++;
		wt += completed.front()->getWaitTime();
		tt = tt + completed.front()->getWaitTime() + completed.front()->getActiveTime();
		#ifdef printProcesses
			completed.front()->toString();
		#endif
		completed.pop();
	}
	wt = wt/np;
	tt = tt/np;
	cout << "Average Wait Time: " << wt << endl;
	cout << "Average Turnaround Time: " << tt << endl;
	cout << "Total Number of Processes Scheduled: " << np << endl;
}

void whs(vector<Process*> processes){
	priority_queue<int, vector<int>, less<int>> active;
	priority_queue<Process*, vector<Process*>, priorityCompare> io;
	queue<Process*> waiting; //processes that havent arrived yet
	queue<Process*> temp; //used as a place holder for when aging
	vector<queue<Process*>> levels;
	map<int, bool> inactive;
	int cycle = 0;
	int numLevels = 100;
	int delim = 50; //delim is used to break up from high and low priority bands
	int ageCap = 5; //get the bottom 10% and then divide by 2 for each band
	int quantum;
	cout << "What would you like the time quantum to be? --> ";
	cin >> quantum;

	//make the list of queues. Each index is it's own priority
	for(int i = 0; i < numLevels; i++){
			levels.push_back(queue<Process*>());
			inactive.emplace(i, true); //create and insert the "inactive queue"
	}
	
	//bring in processes into waiting queue
	for(Process* p : processes){
		waiting.push(p);
	}
	
	while(!waiting.empty() || inactive.size() < 100){ //TODO better conditional
		
		//check for new arrivals
		if(waiting.front() != NULL && waiting.front()->getArrival() == cycle){
			int pri = waiting.front()->getPriority();
			
			//check if that priority is inactive
			if(inactive.count(pri) > 0){
				inactive.erase(pri); //take the queue out of its inactive state
				active.push(pri);
			}
			
			levels.at(pri).push(waiting.front());
			
			waiting.pop();
		}
		
		//bring back in any IO completions
		if(!io.empty()){
			cout << "I/O not empty: " << cycle << endl;
			while(io.top()->getIO() <= cycle){
				cout << "I/O bringing back: " << cycle << endl;
				io.top()->setIO(0);
				cout << "Bringing back process :";
				io.top()->toString();
				levels.at(io.top()->getPriority()).push(io.top());
				io.pop();
			}
		}
		//set IO to 0
	
		//do burst
		//get highest priority ready to go
		int numTicks = 1;
		
		while(!active.empty() && levels.at(active.top()).empty()){
			active.pop();
			
		}
		
	
		if(!active.empty()){
			cout << "Tick: " << cycle << endl;
			
			numTicks = quantum;
			
			bool processDone = false;
			if(numTicks + levels.at(active.top()).front()->getActiveTime() >= levels.at(active.top()).front()->getBurst()){
				numTicks = levels.at(active.top()).front()->getBurst() - levels.at(active.top()).front()->getActiveTime();
				
				processDone = true;
			}
			cout << "Done with process done: " << cycle << endl;
			//TODO check if the process does I/O
			bool didIO = false;
			if(levels.at(active.top()).front()->getIO() > 0){
				didIO = true;
				if(numTicks > quantum - 1){
					numTicks = quantum - 1;
					levels.at(active.top()).front()->updateActiveTime(numTicks);
					int newPri = changePriority(levels.at(active.top()).front()->getPriority(), levels.at(active.top()).front()->getIO());
					levels.at(active.top()).front()->setPriority(newPri);
					levels.at(active.top()).front()->setIO(cycle + levels.at(active.top()).front()->getIO());
					io.push(levels.at(active.top()).front());
					levels.at(active.top()).pop(); 
					
					if(levels.at(active.top()).empty()){
						inactive.emplace(active.top(), true);
						int popper = active.top();
						active.pop();
						while(active.top() == popper && !active.empty()){
							active.pop();
						}
					}
				}
			}else if(!processDone){
				levels.at(active.top()).front()->updateActiveTime(numTicks);	
			}
			cout << "Done with I/O: " << cycle << endl;
			//age
			for(int i = 0; i < 5; i++){
				while(!levels.at(i).empty()){
					levels.at(i).front()->updateAge(numTicks);
					//check if ages out
					if(levels.at(i).front()->getAge() >= 100){
						levels.at(i).front()->resetAge();
						int pri = changePriority(levels.at(i).front()->getPriority(), 10);
						levels.at(i).front()->setPriority(pri);
						levels.at(pri).push(levels.at(i).front());
						//check if empty
						if(levels.at(pri).empty())
							inactive.emplace(pri, true);
					}
					else
						temp.push(levels.at(i).front());
					
					levels.at(i).pop();
				}
				while(!levels.at(i+50).empty()){
					levels.at(i+50).front()->updateAge(numTicks);
					//check if ages out
					if(levels.at(i+50).front()->getAge() >= 100){
						levels.at(i+50).front()->resetAge();
						int pri = changePriority(levels.at(i+50).front()->getPriority(), 10);
						levels.at(i+50).front()->setPriority(pri);
						levels.at(pri).push(levels.at(i+50).front());
						//check if empty
						if(levels.at(pri).empty())
							inactive.emplace(pri, true);
					}
					else
						temp.push(levels.at(i+50).front());
					
					levels.at(i+50).pop();
				}
			}
			cout << "Done with aging: " << cycle << endl;
			while(!temp.empty()){
				if(inactive.count(temp.front()->getPriority()) > 0){
					inactive.erase(temp.front()->getPriority());
				}
				levels.at(temp.front()->getPriority()).push(temp.front());
				temp.pop();
			}
			
			if(!processDone){//Demotion
					levels.at(active.top()).front()->resetAge();
					int pri = changePriority(levels.at(active.top()).front()->getPriority(), numTicks * -1);
					levels.at(active.top()).front()->setPriority(pri);
					levels.at(pri).push(levels.at(active.top()).front());
					levels.at(active.top()).pop();
			}
			else if(!didIO){
				levels.at(active.top()).front()->updateActiveTime(numTicks);
				levels.at(active.top()).front()->setStatus("Completed!");
				levels.at(active.top()).front()->toString();
				levels.at(active.top()).pop(); //TODO move into done queue
				if(levels.at(active.top()).empty()){
					inactive.emplace(active.top(), true);
					int popper = active.top();
					active.pop();
					while(active.top() == popper && !active.empty()){
						active.pop();
					}
				}
			}
			
			
		}
		//do i/o, priority also ticks up the number of i/o cycles completed
		//do queue changes
		//when a process gets into its io stage, set io to cycle+io and update priority by ioBurst
		//high priority = 50-99, low is 0-49, processes stay in their own bands
		//io is done at quantum - 1, when it is brought back, set the io to 0
		
		cycle += numTicks;
		while(!temp.empty()){
			temp.pop();
		}
	}
}
int main(int argc, char* argv[]){
	vector<Process*> processes;
	/*
	to instantiate a priority queue, do it like so: priority_queue<Process, howToSort> processes;
	may need to do it like this instead: priority_queue<Process, vector<Process>, howToSort> processes; 
	*/
	string fileName;
	cout << "Please enter the file location you would like us to read from --> "; //need to factor in for if they just manually want to enter them in
	cin >> fileName;
	processes = parseFile(fileName);
	
	string scheduler;
	cout << "What type of scheduler would you like to utilize (rts, mfqs, whs)? --> "; 
	cin >> scheduler;
	
	char addingProcesses;
	cout << "Would you like to add any other processes into your " << scheduler << " scheduler (y/n)? --> ";
	cin >> addingProcesses;
	if(addingProcesses =='y'){
		int numNewProcesses, pid, burst, arrival, priority, deadline, io;
		cout << "How many processes would you like to add in? --> ";
		cin >> numNewProcesses;
		for(int i=0;i<numNewProcesses;i++){
			cout <<"Enter in the id for process " << i << " --> ";
			cin >> pid;
			cout <<"Enter in the burst for process " << i << " --> ";
			cin >> burst;
			cout <<"Enter in the arrival for process " << i << " --> ";
			cin >> arrival;
			cout <<"Enter in the priority for process " << i << " --> ";
			cin >> priority;
			cout <<"Enter in the deadline for process " << i << " --> ";
			cin >> deadline;
			cout <<"Enter in the io count for process " << i << " --> ";
			cin >> io;
			
			processes.push_back(new Process(pid, burst, arrival, priority, deadline, io));
		}
	}
	
	sort(processes.begin(), processes.end(), howToSort);
	cout <<"Number of valid processes: " << processes.size() << "\n\n";
	/* for(int i=0;i<processes.size();i++){
		processes.at(i)->toString();
	} */
		if(scheduler.compare("rts") == 0)
			rts(processes);
		else if(scheduler.compare("whs") == 0)
			whs(processes);
		else
			mfqs(processes);
	return 0;
}





































