#include <vector>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <queue>
#include <string.h>
#include <iostream>
#include <fstream>
#include <map>
using namespace std;

class Process{
	private:
		int pid;
		int burst;
		int arrival;
		int priority;
		int deadline; 
		int io; 
		int waitTime; 
		int activeTime;
		int starting;
		int age;
		string status;
		Process* nextProcess;
	public:
		Process(int npid, int nburst, int narrival, int npriority, int ndeadline, int nio);
		int getPid();
		int getArrival();
		int getWaitTime();
		void updateWaitTime();
		void updateWaitTime(int timing);
		int getActiveTime();
		void updateActiveTime();
		void updateActiveTime(int timing);
		int getPriority();
		void setPriority(int pri);
		int getDeadline();
		int getIO();
		void setIO(int newIO);
		void toString();
		int getBurst();
		string getStatus();
		void setStatus(string status);
		void setStartTime(int tick);
		int getStartTime();
		void updateAge();
		void updateAge(int aging);
		void resetAge();
		int getAge();
		bool canFinish(int cycle);
		void next(Process* process);
		bool done();
		Process* next();
};
